const lineItems = {
    lineItems: [
        {
            quantity: 1,
            productId: 103,
        },
        {
            quantity: 1,
            productId: 81,
        },
        {
            quantity: 1,
            productId: 77,
            variantId: 11,
        },
    ],
};
function redirectToCart() {
    window.location = '/cart.php';
}
function postData(url = '', cartItems = {}) {
    return fetch(url, {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(cartItems),
    }).then(() => {
        redirectToCart();
    });
}
function setStyleToBlock(elt) {
    const element = elt;
    element.style.display = 'block';
}
function showLoadingOverlay() {
    document.getElementsByClassName('loadingOverlay').forEach(setStyleToBlock);
}
function addBundleToCart(context) {
    const cartID = context.cartId;
    let url = '/api/storefront/cart';
    if (cartID !== null) {
        // cart wasn't empty, update url
        showLoadingOverlay();
        url = `/api/storefront/carts/${cartID}/items`;
    }
    postData(url, lineItems)
        .then(data => console.log(JSON.stringify(data)))
        .catch(error => console.error(error));
}
export default function hookBundleButton(context) {
    $('#addToCart').click(() => {
        addBundleToCart(context);
    });
}
